require 'rubygems'
require 'anemone'

START = ARGV.shift
OUTDIR = ARGV.shift + '/'

def doc_to_text(doc, io)
  doc.xpath('//text()').each do |txt|
    io.write txt.content.chomp
  end
end

puts "Start: #{START} Outdir: #{OUTDIR}"

counter = 0

index = File.open(OUTDIR + 'MANIFEST', 'a')

index.write "# start: #{START}\n"

Anemone.crawl(START) do |anemone|
  
  anemone.skip_links_like(/index.php/ , /Talk/ , /Special/ , /#/ , /repo/ , /%25/ , /%23/ , /javadoc/ )
  
  anemone.focus_crawl { |page| page.links.find_all { |l| l.to_s =~ /^#{Regexp.quote(START)}/ } }
  
  anemone.on_every_page do |page|
      puts "#{page.code}\t#{page.url}"
      filename = page.url.host + '_' + ('%03d' % counter)
      index.write "#{page.code}\t#{page.url}\t"
      if page.code / 100 == 2
        index.write filename
        file = File.open(OUTDIR + filename, 'w')
        if page.doc
          doc_to_text page.doc , file
        else
          file.write page.body
        end
        file.close
      end
      index.write "\n"
      counter += 1
  end
end

index.close