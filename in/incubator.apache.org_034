

  
  Apache Jena - Jena assembler quickstart
  
  
  

  
    
    
    

    
      Apache Jena
      
        
        Home
        Support
        Getting started
        Tutorials
        Documentation
        
      
    
  

  
  Quick linksHomeDownloadsHelp and supportReport a bugRoadmapGetting involvedDocumentationAbout JenaHomeAbout JenaArchitectureRoadmapProject teamRelated projectsDownloadDownloading JenaUsing MavenUsing OSGiHelp and supportGetting helpBugs and suggestionsGetting StartedA first Jena projectRDF API overviewQuerying RDF with SPARQLServing RDF over HTTPTell me how to ...TutorialsTutorials indexRDF tutorialSPARQL queriesUsing Jena with EclipseDocumentationOverviewJavadocRDFSPARQL (ARQ)Application APICommand line utilitiesTDBAPI for TransactionsDataset AssemblerSDBLARQFuseki: Serving DataOntologyInferenceAssemblerAssembler how-toInside assemblersI/OI/O how-toRIOTNotesConcurrency how-toEvent handler how-toFile manager how-toModel factory how-toRDF framesReification how-toTyped literals how-toSupport for IRI'sToolsschemageneyeballGetting InvolvedContributing to JenaASF linksApache Software FoundationLicenseThanksBecome a SponsorSecurity
  

  
    Jena assembler quickstart
    Jena's assembler provides a means of constructing Jena models
according to a recipe, where that recipe is itself stated in
RDF. This is the Assembler quickstart page. For more detailed
information, see the Assembler howto
or Inside assemblers.What is an Assembler specification?An Assembler specification is an RDF description of how to
construct a model and its associated resources, such as reasoners,
prefix mappings, and initial content. The Assembler vocabulary is
given in the Assembler schema,
and we'll use the prefix ja for its identifiers.What is an Assembler?An Assembler is an object that implements the Assembler
interface and can construct objects (typically models) from
Assembler specifications. The constant Assembler.general is an
Assembler that knows how to construct some general patterns
of model specification.How can I make a model according to a specification?Suppose the Model M contains an Assembler specification whoseroot - the Resource describing the whole Model to construct isR (so R.getModel() == M). Invoke:Assembler.general.openModel(R)

The result is the desired Model. Further details about theAssembler interface, the special Assembler general, and the
details of specific Assemblers, are deferred to theAssembler howto.How can I specify ...In the remaining sections, the object we want to describe is given
the root resource my:root.... a memory model?my:root a ja:MemoryModel.

... an inference model?my:root
    ja:reasoner [ja:reasonerURL theReasonerURL] ;
    ja:baseModel theBaseModelResource
    .

theReasonerURL is one of the reasoner (factory) URLs given in the
inference documentation and code; theBaseModelResource is another
resource in the same document describing the base model.... some initialising content?my:root
    ja:content [ja:externalContent <someContentURL>]
    ... rest of model specification ...
    .

The model will be pre-loaded with the contents of someContentURL.... an ontology model?my:root
    ja:ontModelSpec ja:OntModelSpecName ;
    ja:baseModel somebaseModel
    .

The OntModelSpecName can be any of the predefined Jena
OntModelSpec names, eg OWL_DL_MEM_RULE_INF. The baseModel is
another model description - it can be left out, in which case you
get an empty memory model. SeeAssembler howto for construction of
non-predefined OntModelSpecs.
  

  
    
      
        Copyright © 2011–12 The Apache Software Foundation, Licensed under
        the Apache License, Version 2.0.
        
        Apache Jena, Jena, the Apache Jena project logo,
        Apache and the Apache feather logos are trademarks of The Apache Software Foundation.
      
    
  


