MANIFEST = ARGV.shift
FILE_INDEX = ARGV.shift
RESULTS = ARGV.shift
FROM_DOMAIN = ARGV.shift

STDERR.puts "Step 1: Build file -> URI index using #{MANIFEST}"

file_to_uri = {}

File.open(MANIFEST).each_line do |line|
  r = /200\t(\S+)\t(\S+)/.match line
  if r
    # filename => uri
    file_to_uri[r[2]] = r[1]
  end
end

STDERR.puts "Step 2: Build num -> URI index using #{FILE_INDEX}"

num_to_uri = {}

File.open(FILE_INDEX).each_line do |line|
  r = /Key:\s+(\d+):\s+Value:\s+\/(\S+)/.match line
  if r
    # num => filename => uri
    #puts "#{r[1]} => #{file_to_uri[r[2]]}"
    num_to_uri[r[1]] = file_to_uri[r[2]]
  end
end

file_to_uri = nil # won't need it again

STDERR.puts "Step 3: Build URI -> scored matches using #{RESULTS}"

def pull_scores(from, scoreline, n2u)
  # num:score,num:score -> uri => score , uri => score
  # hack! assumes scores are not keys to uri (safe, I think)
  scores = Hash[ * scoreline.split(/[,:]/).map {|v|  n2u.fetch(v, v.to_f)} ]
  scores.reject { |k, v| !k || k.start_with?(from) }
end

def order_by_value(hash)
  hash.keys.sort { |a, b| hash[a] <=> hash[b] }
end

File.open(RESULTS).each_line do |line|
  r = /(\d+)\s+\{(\S+)\}/.match line
  if r
    uri = num_to_uri[r[1]]
    next unless (uri && uri.start_with?(FROM_DOMAIN))
    uri_with_scores = pull_scores(FROM_DOMAIN, r[2], num_to_uri)
    puts "#{uri} ->"
    order_by_value(uri_with_scores).reverse.each do |value|
      puts "    #{value} (#{uri_with_scores[value]})"
    end
    
  end
end